const fs = require('fs');

//Let's write
const fileOperation = require('./task.js');
fileOperation.write('new-file.txt', 'This content is written from file.js module for practice okkkk')
.then((data) => {
    console.log("File written successful ", data)
})
.catch((err) => {
    console.log("File written unsuccessful ", err)
})

//let's rename file
fs.rename('./files/new_file.txt', './files/new-file.txt', (err, result) => {
    if(err){
        console.log(err)
        return
    }
    console.log(result);
})

// let's remove some file
fs.unlink('./files/new-file', (err, result) => {
    if(err){
        console.log(err);
        return
    }
    console.log(result);
})

//Let's read file
// fs.readFile('./files/newTask.txt', 'UTF-8', (err, result) => {
//     if(err){
//         console.log(err);
//         return
//     }
//     const toReadFile = result;
//     console.log(toReadFile);
// })
fileOperation.read('jenis.txt');