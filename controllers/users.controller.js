const router = require('express').Router();
const { ObjectId } = require('mongodb');
const dbConfig = require('./../cofigs/db.config');
function db_connect(cb){
    dbConfig.MongoClient.connect(dbConfig.conxnURL, { useUnifiedTopology: true }, (err, client) => {
        if(err) {
            cb(err, null)
        }else{
            const selectDb = client.db(dbConfig.dbName);
            cb(null, selectDb)
        }
     } )
    //  It is similar to mysqli_connect in php we just type db host and dbName in here
}
router.route('/')
    .get((req,res,next) => {
       db_connect((err, db) => {
           if(err){
               console.log("DB CONNection err")
               return next(err)
           }
           db
           .collection('users')
           .find({})
           .toArray()
           .then((data) => {
               res.json(data);
           })
           .catch((err) => {
               next(err);
           })
       })
    })
    .post((req,res,next) => {

    })
    .put((req,res,next) => {

    })
    .delete((req,res,next) => {

    });

router.route('/search')
    .get((req,res,next) => {
        console.log('Hello from Search of user');
        require('fs').readFile(`./files/${req.query.fileName}`, 'UTF-8', (err, result) =>{
            if(err){
                console.log('fileName ==> ', req.query.fileName)
                next(err);
                return
            }
            res.send(result);
        })
    })
    .post((req,res,next) => {

    })
    .put((req,res,next) => {

    })
    .delete((req,res,next) => {

    });

router.route('/:id')
    .get((req,res,next) => {
        // res.json({
        //     msg : 'Hello from dynamic id of users',
        //     data : req.params.id
        // })
        db_connect((err, db) => {
            if(err){
                return next(err);
            }
            db.
            collection('users')
            .find({
                _id: new dbConfig.OID(req.params.id)
            })
            .toArray()
            .then((data) => {
                res.json(data);
            })
            .catch((err) => {
                next(err);
            })
        })
    })
    .post((req,res,next) => {

    })
    .put((req,res,next) => {
        db_connect((err, db) => {
            if(err){
                return next(err);
            }
            db
            .collection('users')
            .update({
                _id: new dbConfig.OID(req.params.id)
            }, {
                $set: req.body
            })
            .then((data) => {
                res.json(data);
            })
            .catch((err) => {
                next(err);
            })
        })
    })
    .delete((req,res,next) => {
        db_connect((err, db) => {
            if(err){
                return next(err);
            }
            db
            .collection('users')
            .remove({
                _id: new dbConfig.OID(req.params.id)
            })
            .then((data) => {
                res.json(data);
            })
            .catch((err) => {
                next(err);
            })
        })
    });    
module.exports = router;