const router = require('express').Router();
router.route('/')
    .get((req,res,next) => {
        res.send('Hello from users')
    })
    .post((req,res,next) => {

    })
    .put((req,res,next) => {

    })
    .delete((req,res,next) => {

    });

router.route('/search')
    .get((req,res,next) => {
        console.log('Hello from Search of user');
        require('fs').readFile(`./files/${req.query.fileName}`, 'UTF-8', (err, result) =>{
            if(err){
                console.log('fileName ==> ', req.query.fileName)
                next(err);
                return
            }
            res.send(result);
        })
    })
    .post((req,res,next) => {

    })
    .put((req,res,next) => {

    })
    .delete((req,res,next) => {

    });

router.route('/:id')
    .get((req,res,next) => {
        res.json({
            msg : 'Hello from dynamic id of users',
            data : req.params.id
        })
    })
    .post((req,res,next) => {

    })
    .put((req,res,next) => {

    })
    .delete((req,res,next) => {

    });

    
module.exports = router;