const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    res.render('index.pug', {
        message : 'Welcome to nodejs with express',
        title : 'Node js'
    });
    // require('fs').readFile('sdsd', (err, result) => {
    //     if(err){
    //         next(err)
    //         return
    //     }
    //     res.send(result)
    // })
})
router.get('/user', (req, res, next) => {
    // var options = {
    //     root: path.join(__dirname, 'views')
    // }
    res.sendFile(process.cwd()+'/views'+'/test.html', (err) => {
        console.log('err ==> ',err);
    })
})
router.get('/login', (req,res,next) => {
    res.render('login.pug');
})
router.post('/login', (req, res, next) => {
    res.send('Hello from post login of auth');
})
router.get('/register', (req,res,next) => {
    res.render('register.pug');
})
router.post('/register', (req,res,next) => {
    res.render('reg-response.pug', {
        full_name : req.body.full_name,
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    })
})

module.exports = router;