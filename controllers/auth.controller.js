const express = require('express');
const router = express.Router();
const dbConfig = require('./../cofigs/db.config');

function db_connect(cb){
    dbConfig.MongoClient.connect(dbConfig.conxnURL, { useUnifiedTopology: true }, (err, client) => {
        if(err) {
            cb(err, null)
        }else{
            const selectDb = client.db(dbConfig.dbName);
            cb(null, selectDb)
        }
     } )
    //  It is similar to mysqli_connect in php we just type db host and dbName in here
}
router.get('/login', (req,res,next) => {
    res.render('login.pug');
})
router.post('/login', (req, res, next) => {
    const data = req.body;
    // MongoClient.connect(conxnURL, { useUnifiedTopology: true }, (err, client) => {
    //     if(err) {
    //         return next(err);
    //     }
    //     const db = client.db(dbName);
    //     db.collection('users').find({

    //     })
    //     .toArray()
    //     .then((data) => {
    //         res.json(data);
    //     })
    //     .catch((err) => {
    //         next(err);
    //     })
    // });
     db_connect((err, db) => {
         if(err){
             return next(err);
         }
         db
         .collection('users')
         .find({})
         .toArray()
         .then((data) => {
             res.json(data);
         })
         .catch((err) => {
             next(err);
         })
     })
})

router.get('/register', (req,res,next) => {
    res.render('register.pug');
})
router.post('/register', (req,res,next) => {
    //req.body will have data from client
    //database operation
    // MongoClient.connect(conxnURL, { useUnifiedTopology: true }, (err, client) => {
    //     if(err){
    //         next(err);
    //         return
    //     }
    //     console.log("Databse connection successfull");
    //     const selectDb = client.db(dbName);
    //     selectDb.collection('users')
    //     .insert(req.body)
    //     .then((data) => {
    //         res.json(data);
    //         client.close();
    //     })
    //     .catch((err) => {
    //         next(err);
    //     })
    // })
    db_connect((err, db) => {
        if(err){
            console.log('Error in connection')
            return next(err);
        }
        db.collection('users')
        .insert(req.body)
        .then((data) => {
            res.json(data);
            client.close();
        })
        .catch((err) => {
            next(err);
        })
    })
})

module.exports = router;