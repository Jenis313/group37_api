const express = require('express');
const PORT = 4060;
const app = express();
const fs = require('fs');
const fileOperation = require('./task.js');

//WE don't have to create server in express, express itself provides the server and we just listen
app.get('/home', (req, res) => {
    res.json({
        msg: 'welcome to express server',
        status: 200,
    })
})
app.get('/', (req, res) => {
    res.send("<h1 style='color:red; font-family : sans-serif; width:100vw; height: 100vh; background-color:black; text-align:center';>Hi from Home Page</h1>")
})
//ASSIGNMENT PART STARTS HERE

//1) WRITE
app.get('/write/:file_name/:content', (req, res) =>{
    // console.log(req.params.file_name);
    // console.log(req.params.content);
    fileOperation.write(req.params.file_name, req.params.content)
    .then((data) => {
        res.send(`You have successfully created/updated ${req.params.file_name} and put/replaced your content`);
    })
    .catch((err) => {
        res.send('OOPSS!, WRITE operation failed!' + err);
    })
})

//2) READ
app.get('/read/:file_name', (req, res) =>{
    fs.readFile(`./files/${req.params.file_name}`, 'UTF-8', (err, result) => {
        if(err){
            res.send('READ failed!', err);
            return
        }
        res.send(result);
    })
})

//3) DELETE
app.get('/delete/:file_name', (req, res) =>{
    fs.unlink(`./files/${req.params.file_name}`, (err, result) => {
        if(err){
            res.send('DELETE failed! ', err);
            return
        }
        res.send('File deleted successfully!')
    })
})

//4) RENAME
app.get('/rename/:file_name/:updated_name', (req, res) =>{
    fs.rename(`./files/${req.params.file_name}`, `./files/${req.params.updated_name}`, (err, result) => {
        if(err){
            res.send('RENAME failed! ', err)
            return
        }
        res.send(`RENAME operation successful!!!`);
    })
})


app.listen(PORT, function(err, done){
    if(err){
        console.log('Error in listening ', err)
        return 
    }
    console.log('Server listening at portt ' + PORT);
})