const fs = require('fs');
const http = require('http');
const fileOperation = require('./task');
const server = http.createServer((req, res) => {
    
    //Request or first arg is http request object

    //second or response is http response object
    console.log('client connected to server');
    //request response cycle must be completed
    //Single response can only have one request


    if(req.url === '/write'){
        fileOperation.write('write.txt', ' lets assume this is written from client side')
        .then((data) => {
            res.end("<h1>You have succesfully written something into a file</h1>");
        })
        .catch((err) => {
            res.end("<h1>Sorry !! Writing failed</h1>");
        })
    }else if(req.url === '/read'){
        fs.readFile('./files/newTask.txt', 'UTF-8', (err, result) => {
            if(err){
                res.end("<h1>Sorry !! Cant read the file</h1>");
                return
            }
            res.end(`<h1>${result}</h1>`);
        })
    }
    else{
        res.end("<h1>Nothing to perform</h1>");
    }



    //In HTTP protocal :- for communication we need endpoint(combination of method and url)
    //like GET/login POST/register
});
server.listen('4000', '127.0.0.1', function(err, done){
   if(err){
       console.log('Error in listening ==>', err);
       return
   }
    console.log('Server listening at port 4000 inside')
});