const fs = require('fs');
const http = require('http');
const server = http.createServer((req, res) => {
    let url = req.url;
    let urlArray = url.split('/');
    if(urlArray[1] === 'write'){
        fs.writeFile(`./files/${urlArray[2]}`, urlArray[3], (err, result) => {
            if(err){
                res.end(err);
                return
            }
            res.end(`You have successfully written content into ${urlArray[2]} file`);
        })
    }else if(urlArray[1] === 'read'){
        fs.readFile(`./files/${urlArray[2]}`, 'UTF-8', (err, result) => {
            if(err){
                res.end(err);
                return
            }
            res.end(result);
        })
    }else if(urlArray[1] === 'delete'){
        fs.unlink(`./files/${urlArray[2]}`, (err, result) => {
            if(err){
                res.end(err);
                return
            }
            res.end(`Successfully deleted`);
        })
    }else{
        res.end("Nothing to Perform");
    }


});
server.listen('4050', '127.0.0.1', (err, result) => {
    if(err){
        console.log(err);
        return
    }
    console.log("Server Listening at port 4050");
})