const express = require('express');
const PORT = 4070;
const app = express();
const morgan = require('morgan');
const path = require('path');

require('./db_init.js')
//This is like writing db_init js here to make code more cleaner we write it into another place and just require here. We don't save it into any other variables like we do other requires because we haven't imported it. 

//WE don't have to create server in express, express itself provides   the server so we just listen

//Template engine setup
const pug = require('pug');
app.set('view engine', pug);
app.set('views', path.join(process.cwd(), 'views'));


//Import routing level middleware
const authRouter = require('./controllers/auth.controller');
const userRouter = require('./controllers/users.controller');

//Load middlewares
const isAdmin = require('./middleware/isadmin');
const authorize = require('./middleware/authorization')

//Load third party middleware
app.use(morgan('dev'));

//Load inbuilt middleware
//Incoming request(Post) must be parsed according to content type
//parser for z-www-form-urlencoded
app.use(express.urlencoded(
    {
        extended: true
    }
))//This middleware will parse incoming request with content type application/x-www-form-urlencodedd

//serve static files
// app.use(express.static('uploads'));
// app.use('/file', express.static('uploads'));


//load routing level middleware
app.use('/auth',isAdmin, authRouter);
app.use('/users', userRouter);

//Error handling middleware
//Error handling middleware must have 4 arguments
//1st argument is for error
//and rest of them are for request, response, and next
//error handling middleware will not come into action in between req-res cycle
//error handling middleware must be called 
//to call error handling middleware we call next with argument
//throughout the application wherever we have scope of next we can call next

app.use((req, res, next) => {
    res.status(400);
    next({
        msg: 'NOT FOUND',
        status: 404
    })
})
app.use((err, req, res, next) => {
    console.log("I'm error handling middleware");
    res.json({
        text: 'Error handling middleware',
        msg : err.msg || err,
        status: err.status || 400
    })
})


app.listen(PORT, function(err, done){
    if(err){
        console.log('Error in listening ', err)
        return 
    }
    console.log('Server listening at portt ' + PORT);
})