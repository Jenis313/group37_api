const { query } = require('express');
const express = require('express');
const PORT = 4040;
const app = express();
const morgan = require('morgan');

//WE don't have to create server in express, express itself provides the server so we just listen
//Load third party middleware
app.use(morgan('dev'));

app.get('/home', (req, res) => {
    res.json({
        msg: 'welcome to express server',
        qry: req.query
    })
})
app.use((req, res, next) => {
    console.log("I am a middleware");
    // res.json({
    //     msg: 'blocked middleware'
    // })
    next();
})
app.use((req, res, next) => {
    console.log("I am second middleware");
    // res.send("Blocked by second middleware");
    next();
})
app.get('/help', (req, res) => {
    res.send("<h1 style='color:red; font-family : sans-serif; width:100vw; height: 100vh; background-color:black; text-align:center';>Hi from Help Page</h1>")
}) 
app.get('/login', (req, res) => {
    res.send("Hi from login page ");
})

app.use((req, res, next) => {
    console.log("I am second middleware");
    // res.send("Blocked by second middleware");
    res.end('Blocked by third middleware! or error 404!');
})
app.listen(PORT, function(err, done){
    if(err){
        console.log('Error in listening ', err)
        return 
    }
    console.log('Server listening at portt ' + PORT);
})