const fs = require('fs');


function myWrite(fileName, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('./files/' + fileName, content, function (err, result) {
            if (err) {
                reject(err);
            }
            else {
                resolve(result)
            }
        })
    })

}
function myRead(fileName){
    fs.readFile(`./files/${fileName}`, 'UTF-8', (err,result) => {
        if(err){
            console.log('Failed to Read');
            return
        }
        console.log(result);
    })
}
module.exports = {
    write: myWrite,
    read: myRead
}
